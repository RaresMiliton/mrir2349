package mrir2349MV.stubs;

import mrir2349MV.model.OutsourcedPart;

public class OutsourcedPartStub extends OutsourcedPart {
    public static OutsourcedPartStub getStub() {
        return new OutsourcedPartStub(1,"Part1", 10.0,5,0,10,"Company");
    }

    private OutsourcedPartStub(int partId, String name, double price, int inStock, int min, int max, String companyName) {
        super(partId, name, price, inStock, min, max, companyName);
    }

    public int getId() {
        return 1;
    }

    public String getName() {
        return "Part1";
    }

    public double getPrice() {
        return 10.0;
    }

    public int getInStock() {
        return 5;
    }

    public int getMin() {
        return 0;
    }

    public int getMax() {
        return 10;
    }

    public String getCompanyName() {
        return "Company";
    }
}

