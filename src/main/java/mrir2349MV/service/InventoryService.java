package mrir2349MV.service;

import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.*;
import mrir2349MV.repository.InventoryRepository;
import javafx.collections.ObservableList;
import mrir2349MV.validator.PartValidator;
import mrir2349MV.validator.ProductValidator;
import mrir2349MV.validator.Validator;

public class InventoryService {

    private InventoryRepository repo;
    private Validator<Part> partValidator = new PartValidator();
    private Validator<Product> productValidator = new ProductValidator();
    public InventoryService(InventoryRepository repo){
        this.repo =repo;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws BusinessException {
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(inhousePart);
        repo.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) throws BusinessException {
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(outsourcedPart);
        repo.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts) throws BusinessException {
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        productValidator.validate(product);
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue) throws BusinessException {
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(inhousePart);
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue) throws BusinessException {
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        partValidator.validate(outsourcedPart);
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts) throws BusinessException {
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        productValidator.validate(product);
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }

}
