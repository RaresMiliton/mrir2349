package mrir2349MV.validator;

import mrir2349MV.exceptions.BusinessException;

public interface Validator<T> {
    public void validate(T elem) throws BusinessException;
}
