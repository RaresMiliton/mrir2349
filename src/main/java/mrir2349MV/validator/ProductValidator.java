package mrir2349MV.validator;

import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.exceptions.BusinessExceptionFactory;
import mrir2349MV.exceptions.ExceptionsEnum;
import mrir2349MV.model.Product;

public class ProductValidator implements Validator<Product> {
    @Override
    public void validate(Product elem) throws BusinessException {
        if(elem.getName().isEmpty()){
            throw BusinessExceptionFactory.getException(ExceptionsEnum.EMPTY_STRINGS);
        }
        if(elem.getPrice() <= 0){
            throw BusinessExceptionFactory.getException(ExceptionsEnum.PRICE_EXCEPTION);
        }
        if(elem.getInStock() <= 0){
            throw BusinessExceptionFactory.getException(ExceptionsEnum.INVALID_STOCK_VALUE);
        }
        if(elem.getMin() < 0 || elem.getMin() > elem.getMax()){
            throw BusinessExceptionFactory.getException(ExceptionsEnum.NUMBER_PIECES_EXCEPTION);
        }
        if(elem.getMax() < 0 || elem.getMin() > elem.getMax()){
            throw BusinessExceptionFactory.getException(ExceptionsEnum.NUMBER_PIECES_EXCEPTION);
        }
    }
}
