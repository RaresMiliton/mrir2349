package mrir2349MV.exceptions;

public class NumberPiecesBusinessException extends BusinessException {
    NumberPiecesBusinessException(String message) {
        super(message);
    }
}
