package mrir2349MV.exceptions;

public class InvalidStockValueException extends BusinessException {
    InvalidStockValueException(String message) {
        super(message);
    }
}
