package mrir2349MV.exceptions;

public enum ExceptionsEnum {
    PRICE_EXCEPTION("Price must be positive like you xD !"),
    NUMBER_PIECES_EXCEPTION("Invalid number of pieces."),
    EMPTY_STRINGS("Please fill the empty strings !"),
    INVALID_STOCK_VALUE("Please try with a positive number between min & max ! ");

    private String message;

    ExceptionsEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType(){
        return this.toString();
    }
}
