package mrir2349MV.exceptions;

public class BusinessException extends Exception {

    private String message;

    BusinessException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
