package mrir2349MV.exceptions;

public class BusinessExceptionFactory {
    public static BusinessException getException(ExceptionsEnum exceptionsEnum){
        switch (exceptionsEnum.getType()){
            case "PRICE_EXCEPTION":{
                return new PriceBusinessException(ExceptionsEnum.PRICE_EXCEPTION.getMessage());
            }
            case "NUMBER_PIECES_EXCEPTION":{
                return new NumberPiecesBusinessException(ExceptionsEnum.NUMBER_PIECES_EXCEPTION.getMessage());
            }
            case "EMPTY_STRINGS":{
                return new EmptyStringException(ExceptionsEnum.EMPTY_STRINGS.getMessage());
            }
            case "INVALID_STOCK_VALUE":{
                return new InvalidStockValueException(ExceptionsEnum.INVALID_STOCK_VALUE.getMessage());
            }
            default:{
                return null;
            }
        }
    }
}
