package mrir2349MV.exceptions;

public class EmptyStringException extends BusinessException {
    EmptyStringException(String message) {
        super(message);
    }
}
