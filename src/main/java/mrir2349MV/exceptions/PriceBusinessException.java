package mrir2349MV.exceptions;

public class PriceBusinessException extends BusinessException {
    PriceBusinessException(String message) {
        super(message);
    }
}
