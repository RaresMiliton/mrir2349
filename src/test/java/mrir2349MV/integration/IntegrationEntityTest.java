package mrir2349MV.integration;


import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.InhousePart;
import mrir2349MV.model.Part;
import mrir2349MV.repository.InventoryRepository;
import mrir2349MV.service.InventoryService;
import org.junit.jupiter.api.*;

import java.io.*;
import java.util.Objects;

public class IntegrationEntityTest {
    private InventoryRepository repo;
    private InventoryService service;
    private String fileName = "test.txt";
    private InhousePart part1;


    @BeforeAll
    static void before_all() {
        System.out.println("----- STARTED UNIT TESTING -----");

    }

    @AfterAll
    static void after_all() {
        System.out.println("----- FINISHED UNIT TESTING -----");
    }

    @BeforeEach
    void setUp() throws BusinessException {
        this.repo = new InventoryRepository(this.fileName);
        this.service = new InventoryService(repo);
        this.part1 = new InhousePart(1, "name1", 0.3, 100, 10, 200, 4);
    }

    @AfterEach
    void tearDown() throws FileNotFoundException {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(this.fileName)).getFile());
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test
    public void addPartInvalidMin() {
        part1.setMin(-5);
        Assertions.assertEquals(service.getAllParts().size(), 0);
        Assertions.assertThrows(BusinessException.class, () -> repo.addPart(part1));
        Assertions.assertEquals(service.getAllParts().size(), 0);
    }

    @Test
    public void addValidPart() throws BusinessException {
        Assertions.assertEquals(service.getAllParts().size(), 0);
        repo.addPart(part1);
        Assertions.assertEquals(service.getAllParts().size(), 1);
    }

    @Test
    public void findPart() throws BusinessException {
        Assertions.assertNull(service.lookupPart("name1"));
        repo.addPart(part1);
        Assertions.assertEquals(service.lookupPart("name1"), part1);
    }
}
