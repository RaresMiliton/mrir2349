package mrir2349MV.integration;


import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.InhousePart;
import mrir2349MV.repository.InventoryRepository;
import mrir2349MV.service.InventoryService;
import mrir2349MV.stubs.OutsourcedPartStub;
import org.junit.jupiter.api.*;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Objects;

import java.io.FileNotFoundException;

public class IntegrationStubTest {
    private InventoryRepository repo;
    private InventoryService service;
    private String fileName = "test.txt";
    private InhousePart part1;


    @BeforeAll
    static void before_all() {
        System.out.println("----- STARTED UNIT TESTING -----");

    }

    @AfterAll
    static void after_all() {
        System.out.println("----- FINISHED UNIT TESTING -----");
    }

    @BeforeEach
    void setUp() throws BusinessException {
        MockitoAnnotations.initMocks(IntegrationStubTest.class);
        this.repo = new InventoryRepository(this.fileName);
        this.service = new InventoryService(repo);
    }

    @AfterEach
    void tearDown() throws FileNotFoundException {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(this.fileName)).getFile());
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test
    public void addValidPart() throws BusinessException {
        OutsourcedPartStub partStub = OutsourcedPartStub.getStub();
        Assertions.assertEquals(service.getAllParts().size(), 0);
        repo.addPart(partStub);
        Assertions.assertEquals(service.getAllParts().size(), 1);
    }

    @Test
    public void findPart() throws BusinessException {
        OutsourcedPartStub partStub = OutsourcedPartStub.getStub();
        Assertions.assertNull(service.lookupPart("Part1"));
        repo.addPart(partStub);
        Assertions.assertEquals(service.lookupPart("Part1"), partStub);
    }
}

