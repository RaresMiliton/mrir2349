package mrir2349MV.repository;


import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.OutsourcedPart;
import mrir2349MV.model.Part;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.mockito.MockitoAnnotations;

public class InventoryRepositoryTestMockito {
    @Mock
    private InventoryRepository repo;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findPart() throws BusinessException {
        Part part = new OutsourcedPart(1, "Part1", 10.0, 5, 0, 10, "Company1");
        Mockito.when(repo.lookupPart("Part1")).thenReturn(null);
        Mockito.doNothing().when(repo).addPart(part);
        Mockito.when(repo.lookupPart("Part1")).thenReturn(part);
    }

    @Test
    public void addPartWithException() throws BusinessException {
        Part part = new OutsourcedPart(1, "Part 1", -5.0, 5, 0, 10, "Company1");
        Mockito.doThrow(BusinessException.class).when(repo).addPart(part);
    }
}
