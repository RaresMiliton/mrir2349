package mrir2349MV.repository;

import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.InhousePart;
import mrir2349MV.service.InventoryService;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class InventoryRepositoryTest {
    private InventoryRepository repo;
    private String fileName = "data/repo_test.txt";
    private InhousePart part1;
    private InhousePart part2;
    private InhousePart part3;
    private InhousePart part4;


    @BeforeAll
    static void before_all() {
        System.out.println("----- STARTED UNIT TESTING -----");

    }

    @AfterAll
    static void after_all() {
        System.out.println("----- FINISHED UNIT TESTING -----");
    }

    @BeforeEach
    void setUp() throws BusinessException {
        this.repo = new InventoryRepository(this.fileName);
        this.part1 = new InhousePart(1, "name1", 0.3, 100, 10, 200, 4);
        this.part2 = new InhousePart(2, "name2", 0.3, 100, 10, 200, 4);
        this.part3 = new InhousePart(3, "name3", 0.3, 100, 10, 200, 4);
        this.part4 = new InhousePart(4, "name4", 0.3, 100, 10, 200, 4);
        repo.addPart(this.part1);
        repo.addPart(this.part2);
        repo.addPart(this.part3);
    }

    @AfterEach
    void tearDown() throws FileNotFoundException {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(this.fileName)).getFile());
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test
    void F02_TC01() {
        Assertions.assertEquals(repo.lookupPart(part1.getName()), part1);
    }

    @Test
    void F02_TC02() {
        Assertions.assertEquals(repo.lookupPart(String.valueOf(part2.getPartId())), part2);
    }

    @Test
    void F02_TC03() {
        Assertions.assertNull(repo.lookupPart(part4.getName()));
    }

    @Test
    void F02_TC04() {
        Assertions.assertNull(repo.lookupPart(""));

    }

    @Test
    void F02_TC05() {
        Assertions.assertEquals(repo.lookupPart(part2.getName()), part2);
    }
}