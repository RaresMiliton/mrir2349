package mrir2349MV.service;

import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.InhousePart;
import mrir2349MV.model.Part;
import mrir2349MV.repository.InventoryRepository;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTest {
    private InventoryRepository repo;
    private InventoryService service;
    private String fileName = "data/repo_test.txt";
    private InhousePart part;


    @BeforeAll
    static void before_all() {
        System.out.println("----- STARTED UNIT TESTING -----");

    }

    @AfterAll
    static void after_all() {
        System.out.println("----- FINISHED UNIT TESTING -----");
    }

    @BeforeEach
    void setUp() {
        repo = new InventoryRepository(this.fileName);
        service = new InventoryService(repo);
        this.part = new InhousePart(1, "name", 0.3, 100, 10, 200, 4);
    }

    @AfterEach
    void tearDown() throws FileNotFoundException {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(this.fileName)).getFile());
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test
    void ECP_TEST1() {
        try {
            service.addInhousePart( "name", 0.3, 100, 10, 200, 4);
            assertEquals(1, service.getAllParts().size());
        } catch (BusinessException e) {
            Assertions.assertTrue(false);
        }
    }

    @Test
    void ECP_TEST2() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "", 0.3, 100, 10, 200, 4));
    }

    @Test
    void ECP_TEST3() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", -0.3, 100, 10, 200, 4));
    }

    @Test
    void ECP_TEST4() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0.3, -100, 10, 200, 4));
    }

    @Test
    void ECP_TEST5() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0.3, 300, 10, 200, 4));
    }

    @Test
    void ECP_TEST6() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0.3, 3, 10, 200, 4));
    }

    @Test
    void ECP_TEST7() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0.3, 100, 10, -200, 4));
    }

    @Test
    void ECP_TEST8() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0.3, 100, 1000, 200, 4));
    }
    @Test
    void ECP_TEST9() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0.3, 10, -1000, 200, 4));
    }


    @Test
    void BVA_TEST1() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", 0, 100, 10, 200, 4));
    }

    @Test
    void BVA_TEST2() {
        try {
            service.addInhousePart( "name", 1, 100, 10, 200, 4);
            assertEquals(1, service.getAllParts().size());
        } catch (BusinessException e) {
            Assertions.assertTrue(false);
        }
    }

    @Test
    void BVA_TEST3() {
        Assertions.assertThrows(BusinessException.class, () -> service.addInhousePart( "name", -1, 100, 10, 200, 4));
    }

    @Test
    void BVA_TEST4() {
        try {
            service.addInhousePart( "name", Double.MAX_VALUE, 100, 10, 200, 4);
            assertEquals(1, service.getAllParts().size());
        } catch (BusinessException e) {
            Assertions.assertTrue(false);
        }
    }

    @Test
    void BVA_TEST5() {
        try {
            service.addInhousePart( "name", Double.MAX_VALUE+1, 100, 10, 200, 4);
            assertEquals(1, service.getAllParts().size());
        } catch (BusinessException e) {
            Assertions.assertTrue(false);
        }
    }

    @Test
    void BVA_TEST6() {
        try {
            service.addInhousePart( "name", Double.MAX_VALUE-1, 100, 10, 200, 4);
            assertEquals(1, service.getAllParts().size());
        } catch (BusinessException e) {
            Assertions.assertTrue(false);
        }
    }


}