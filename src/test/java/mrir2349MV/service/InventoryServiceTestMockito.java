package mrir2349MV.service;

import mrir2349MV.exceptions.BusinessException;
import mrir2349MV.model.OutsourcedPart;
import mrir2349MV.model.Part;
import mrir2349MV.repository.InventoryRepository;
import org.junit.jupiter.api.*;
import org.mockito.*;

import static org.mockito.Mockito.times;

public class InventoryServiceTestMockito {

    @Mock
    private InventoryRepository repo;

    @InjectMocks
    private InventoryService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addValidPart() throws BusinessException {
        Part part = new OutsourcedPart(0,"Part1", 10.0, 5, 0, 10, "Company1");
        Mockito.verify(repo, times(0)).addPart(part);
        service.addOutsourcePart("Part1", 10.0, 5, 0, 10, "Company1");
        Mockito.verify(repo, times(1)).addPart(part);
    }

    @Test
    public void addPartInvalidPrice() throws BusinessException {
        Part part = new OutsourcedPart(0,"Part1", 10.0, 5, 0, 10, "Company1");
        Mockito.verify(repo, times(0)).addPart(part);
        try{
            service.addOutsourcePart("Part1", -5, 5, 0, 10, "Company1");
        }catch (BusinessException be){
        }
        Mockito.verify(repo, times(0)).addPart(part);
    }

}
